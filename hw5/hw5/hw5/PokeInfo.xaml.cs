﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw5
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PokeInfo : ContentPage
	{
        public PokeInfo()
		{
			InitializeComponent();
		}

        public PokeInfo(Pokemon pokemon)
        {
            InitializeComponent();
            BindingContext= pokemon;
        }

        private void Handle_PokeInfoButtonClicked(object sender, EventArgs e)
        {
            Pokemon pokeMenuSelected = (Pokemon)BindingContext;
            Uri pokeInfoUri = new Uri(pokeMenuSelected.PokeUrl);
            Device.OpenUri(pokeInfoUri);
        }
    }
}