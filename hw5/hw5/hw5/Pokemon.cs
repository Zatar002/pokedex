﻿using Xamarin.Forms;

namespace hw5
{
    public class Pokemon
    {
        public string PokeName
        {
            get;
            set;
        }
        public string PokeDexEntry
        {
            get;
            set;
        }
        public ImageSource PokeImage
        {
            get;
            set;
        }
        public string PokeUrl
        {
            get;
            set;
        }
        public string PokeType
        {
            get;
            set;
        }
        public string PokeHeight
        {
            get;
            set;
        }
        public string PokeWeight
        {
            get;
            set;
        }
    }
}
