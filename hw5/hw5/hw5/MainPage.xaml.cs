﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace hw5
{
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<Pokemon> myPokeCollection;
        
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);  //THIS IS THE IDEAL LOCATION (underneath InitializeComponent
            PopulateListView();
            
        }

        /*private void MyPokeList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            ListView tappedPokeListView = (ListView)sender;
            Pokemon tappedPokemon = (Pokemon)tappedPokeListView.SelectedItem;
            Uri tappedPokeUri = new Uri(tappedPokemon.PokeUrl);
            Device.OpenUri(tappedPokeUri);
        }*/

        public void PopulateListView()
        {
            myPokeCollection = new ObservableCollection<Pokemon>()
            {
                new Pokemon()
                {
                    PokeName = "Bulbasaur",
                    PokeDexEntry = "#001, the 'Seed Pokémon'",
                    PokeImage =ImageSource.FromFile("bulbasaursmall.png"),   //can't name images by single number eg "1.png", causes problems with android
                    PokeUrl = "https://pokemondb.net/pokedex/bulbasaur",
                    PokeType = "GRASS POISON",
                    PokeHeight = "2′04″ (0.7 m)",
                    PokeWeight = "15.2 lbs (6.9 kg)",

                },
                new Pokemon()
                {
                    PokeName = "Ivysaur",
                    PokeDexEntry = "#002, the 'Seed Pokémon'",
                    PokeImage =ImageSource.FromFile("ivysaursmall.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/ivysaur",
                    PokeType = "GRASS POISON",
                    PokeHeight = "3′03″ (1.0 m)",
                    PokeWeight = "28.7 lbs (13.0 kg)",
                },
                new Pokemon()
                {
                    PokeName = "Venusaur",
                    PokeDexEntry = "#003, the 'Seed Pokémon'",
                    PokeImage =ImageSource.FromFile("venusaursmall.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/venusaur",
                    PokeType = "GRASS POISON",
                    PokeHeight = "6′07″ (2.0 m)",
                    PokeWeight = "220.5 lbs (100.0 kg)",
                },
                new Pokemon()
                {
                    PokeName = "Charmander",
                    PokeDexEntry = "#004, the 'Lizard Pokémon'",
                    PokeImage =ImageSource.FromFile("charmandersmall.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/charmander",
                    PokeType = "FIRE",
                    PokeHeight = "2′00″ (0.6 m)",
                    PokeWeight = "18.7 lbs (8.5 kg)",
                },
                new Pokemon()
                {
                    PokeName = "Charmeleon",
                    PokeDexEntry = "#005, the 'Flame Pokémon'",
                    PokeImage =ImageSource.FromFile("charmeleonsmall.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/charmeleon",
                    PokeType = "FIRE",
                    PokeHeight = "3′07″ (1.1 m)",
                    PokeWeight = "41.9 lbs (19.0 kg)",
                },
                new Pokemon()
                {
                    PokeName = "Charizard",
                    PokeDexEntry = "#006, the 'Flame Pokémon'",
                    PokeImage =ImageSource.FromFile("charizardsmall.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/charizard",
                    PokeType = "FIRE FLYING",
                    PokeHeight = "5′07″ (1.7 m)",
                    PokeWeight = "199.5 lbs (90.5 kg)",
                },
                new Pokemon()
                {
                    PokeName = "Squirtle",
                    PokeDexEntry = "#007, the 'Tiny Turtle Pokémon'",
                    PokeImage =ImageSource.FromFile("squirtlesmall.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/squirtle",
                    PokeType = "WATER",
                    PokeHeight = "1′08″ (0.5 m)",
                    PokeWeight = "19.8 lbs (9.0 kg)",
                },
                new Pokemon()
                {
                    PokeName = "Wartortle",
                    PokeDexEntry = "#008, the 'Turtle Pokémon'",
                    PokeImage =ImageSource.FromFile("wartortlesmall.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/wartortle",
                    PokeType = "WATER",
                    PokeHeight = "3′03″ (1.0 m)",
                    PokeWeight = "49.6 lbs (22.5 kg)",
                },
                new Pokemon()
                {
                    PokeName = "Blastoise",
                    PokeDexEntry = "#009, the 'Shellfish Pokémon'",
                    PokeImage =ImageSource.FromFile("blastoisesmall.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/blastoise",
                    PokeType = "WATER",
                    PokeHeight = "5′03″ (1.6 m)",
                    PokeWeight = "188.5 lbs (85.5 kg)",
                },
            };

            myPokeListView.ItemsSource = myPokeCollection;
        }

        public void ClearListView()
        {
            myPokeCollection.Clear();
        }

        async void Handle_ContextMenuInfoButton(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var pokeMenuSelected = (Pokemon)menuItem.CommandParameter;
            await Navigation.PushAsync(new PokeInfo(pokeMenuSelected));
        }

        private void Handle_ContextMenuDeleteButton(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var pokeMenuSelected = (Pokemon)menuItem.CommandParameter;
            DisplayAlert("Deleted:", pokeMenuSelected.PokeName, "OK");

            myPokeCollection.Remove(pokeMenuSelected);
        }

        private void Handle_myPokeListViewRefreshing(object sender, EventArgs e)
        {
            var pokeListView_toRefresh = (ListView)sender;
            ClearListView();
            PopulateListView();
            myPokeListView.IsRefreshing = false;
        }
    }
}
